import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import { StoreType } from 'core/store';
import { Visibility, ServiceType } from 'data/enum';
import { EventType } from 'data/event/model';
import { Button } from 'shared/base/button';
import { Line } from 'shared/base/line';
import { DateTime } from 'shared/base/utils/dateTime';
import { priority, type } from 'app/translations';
import { getAddressAsync } from 'data/event/action';
import { Modal } from 'shared/base/modal';
import { TextBoxField } from 'shared/fields/textBoxField';

import './card.scss';

export interface ButtonProps extends React.HTMLAttributes<any> {
  event: EventType;
  onEdit: () => void;
  onDelete: () => void;
  onApprove: () => void;
}

export const Card: React.FC<ButtonProps> = ({ event, onEdit, onDelete, onApprove }) => {
  const currentMode = useSelector((state: StoreType) => state.event.currentMode);
  const dispatch = useDispatch();
  const [address, setAddress] = useState("");
  const [approveDialog, setApproveDialog] = useState(false);
  const [rejectDialog, setRejectDialog] = useState(false);
  const [deleteDialog, setDeleteDialog] = useState(false);

  useEffect(() => {
    dispatch(getAddressAsync({
      lon: event.lon,
      lat: event.lat,
      onResponseCallback: (v: any) => setAddress(v)
    }, `${event.lon}${event.lat}`));
  }, [dispatch]);


  const footerApprove = (
    <Line>
      <Button className="btn-outline-primary btn-sm" onClick={onApprove}>Подтвердить</Button>
      <Button className="btn-outline btn-sm cancel-btn" onClick={() => setApproveDialog(false)}>Отменить</Button>
    </Line>
  );

  const footerReject = (
    <Line>
      <Button className="btn-outline-danger btn-sm">Отклонить</Button>
      <Button className="btn-outline btn-sm cancel-btn" onClick={() => setRejectDialog(false)}>Отменить</Button>
    </Line>
  );

  const footerDelete = (
    <Line>
      <Button className="btn-outline-danger btn-sm">Удалить</Button>
      <Button className="btn-outline btn-sm cancel-btn" onClick={() => setDeleteDialog(false)}>Отменить</Button>
    </Line>
  );

  return (
    <>
      <div className="card container">
        <Line className="card-body">
          <Line vertical mt="3" ml="2">
            <Line className="header" alignItems="baseline">
              <div className={classnames('label', {
                gas: event.type == ServiceType.Gas,
                electricity: event.type == ServiceType.Electricity,
                water: event.type == ServiceType.Water,
                heat: event.type == ServiceType.Heat
              })}></div>
              <div className="title bolder-text">{type.get(event.type)}</div>
            </Line>
            <Line mt="2">
              <Line vertical className="left-column">
                <div className="lighter-text">{DateTime.format(new Date(event.created_at))}</div>
                <Line>
                  <div>Адрес: </div>
                  <div className="lighter-text pl-1">{address}</div>
                </Line>
              </Line>
              <Line vertical>
                <Line>
                  <div>Уровень: </div>
                  <div className="lighter-text pl-1">{priority.get(event.priority)}</div>
                </Line>
                <div className="mt-1">
                  Описание: <span className="lighter-text pl-1">{event.description}</span>
                </div>
              </Line>
            </Line>
          </Line>
          <Line className="col-sm card-column buttons" vertical alignItems="end" mt="3">
            <div className="mt-1">
              {currentMode == Visibility.Pending && (
                <Button className="btn-outline-primary btn-sm mb-2" onClick={() => setApproveDialog(true)}>
                  <div className="button">Подтвердить</div>
                </Button>
              )}
              <Link to={`/edit/${event.id}`}>
                <Button className="btn-outline btn-sm mb-2 edit-button" onClick={onEdit}>
                  <div className="button">Изменить</div>
                </Button>
              </Link>
              <Button className="btn-outline-danger btn-sm"
                onClick={() => currentMode == Visibility.Pending ? setRejectDialog(true) : setDeleteDialog(true)}>
                <div className="button">{currentMode == Visibility.Pending ? 'Отклонить' : 'Удалить'}</div>
              </Button>
            </div>
          </Line>
        </Line>
      </div>
      {approveDialog &&
        <Modal header="Подтверждениe заявки"
          onCancel={() => setApproveDialog(false)}
          footer={footerApprove}
          className="cardModal">
          <div className="text">
            Вы действительно хотите подтвердить заявку?
          </div>
        </Modal>}
      {rejectDialog &&
        <Modal header="Отклонение заявки"
          onCancel={() => setRejectDialog(false)}
          footer={footerReject}
          className="cardModal">
          <div className="text">
            Вы действительно хотите отклонить заявку?<br />
            Если да, то укажите причину
            <TextBoxField
              name="RejectedReason"
              value={""}
              onChange={() => { }}></TextBoxField>
          </div>
        </Modal>}
      {deleteDialog &&
        <Modal header="Удаление заявки"
          onCancel={() => setDeleteDialog(false)}
          footer={footerDelete}
          className="cardModal">
          <div className="text">
            Вы действительно хотите удалить заявку?
          </div>
        </Modal>}
    </>
  );
};
