import { createReducer } from 'core/redux';
import { AuthInfo } from 'data/auth/models';
import { ActionType } from 'data/actionTypes';

interface AuthInitialState {
  authInfo: AuthInfo;
}

const initialState: () => AuthInitialState = () => ({
  authInfo: {
    login: ''
  }
});

export const authReducer = createReducer(initialState, {
  [ActionType.AUTH_SETAUTHINFO]: 'authInfo'
});
