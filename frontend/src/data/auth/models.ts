export interface AuthInfo {
  login: string;
}

export interface User {
  name: string;
  id: number;
}
