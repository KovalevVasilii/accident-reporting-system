import { combineEpics, ofType } from 'redux-observable';
import { map, mergeMap } from 'rxjs/operators';
import { setAuthInfo } from 'data/auth/actions';
import { ActionType } from 'data/actionTypes';
import { loginAsync } from 'data/auth/api';

const loginEpic = (action$: any) => {
  return action$.pipe(
    ofType(ActionType.AUTH_GETAUTHINFOASYNC),
    mergeMap(action => loginAsync().pipe(map(response => setAuthInfo({ login: 'success' }))))
  );
};

export const authEpic = combineEpics(loginEpic);
