import { createAction } from 'core/redux';
import { ActionType } from 'data/actionTypes';

import { AuthInfo } from './models';

export const setAuthInfo = createAction<AuthInfo>(ActionType.AUTH_SETAUTHINFO);
export const getAuthInfo = createAction(ActionType.AUTH_GETAUTHINFOASYNC);
