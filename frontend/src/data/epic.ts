import { combineEpics } from 'redux-observable';

import { authEpic } from './auth/epic';
import { eventEpic } from './event/epic';

export const rootEpic = combineEpics(authEpic, eventEpic);
