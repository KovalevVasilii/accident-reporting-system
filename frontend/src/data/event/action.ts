import { createAction } from 'core/redux';
import { ActionType } from 'data/actionTypes';
import { Visibility } from 'data/enum';
import { EventType } from 'data/event/model';

export const getEventsAsync = createAction(ActionType.EVENT_GETEVENTSASYNC);
export const setEvents = createAction<EventType[]>(ActionType.EVENT_SETEVENTS);
export const setCurrentMode = createAction<Visibility>(ActionType.EVENT_SETCURRENTMODE);
export const getAddressAsync = createAction<{ lon: number, lat: number, onResponseCallback: (v: any) => void }>(ActionType.EVENT_GETADDRESSASYNC);
export const approveEventAsync = createAction<any>(ActionType.EVENT_APPROVEEVENTASYNC);
export const getAllAsync = createAction(ActionType.EVENT_GETALLASYNC);
export const setAll = createAction<EventType[]>(ActionType.EVENT_SETALL);