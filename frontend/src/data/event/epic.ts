import { combineEpics } from 'redux-observable';
import { map, ignoreElements } from 'rxjs/operators';
import { createEpic } from 'core/epic';
import {
  setEvents,
  getEventsAsync,
  getAddressAsync,
  approveEventAsync,
  getAllAsync,
  setAll
} from 'data/event/action';
import { getEvents, getAddress, approveEvent, getAll } from 'data/event/api';
import { Visibility } from 'data/enum';

const getEventsEpic = createEpic(getEventsAsync, (mode: Visibility) => {
  return getEvents(mode).pipe(
    map(response => {
      return setEvents(response.data);
    })
  );
});

const getAddressEpic = createEpic(getAddressAsync, (data: { lon: number, lat: number, onResponseCallback: (v: any) => void }) => {
  return getAddress(data.lon, data.lat).pipe(
    map(response => {
      return data.onResponseCallback((response as any).response.GeoObjectCollection.featureMember[0].GeoObject.name);
    }),
    ignoreElements()
  );
});

const approveEventEpic = createEpic(approveEventAsync, (event: any) => {
  return approveEvent(event).pipe(
    ignoreElements()
  );
});

const getAllEpic = createEpic(getAllAsync, () => {
  return getAll().pipe(
    map(response => {
      return setAll(response.data);
    })
  );
});

export const eventEpic = combineEpics(getEventsEpic, getAddressEpic, approveEventEpic, getAllEpic);