import { User } from 'data/auth/models';
import { Status, Priority, ServiceType } from 'data/enum';

export interface EventType {
  description: string;
  type: ServiceType;
  lon: number;
  lat: number;
  priority: Priority;
  status: Status;
  id: number;
  created_at: string;
  deadline: string;
  start_time: string;
  user: User;
}

export interface EventChanged {
  id: number;
  description: string;
  lon: number;
  lat: number;
  is_system: boolean;
  rejected_reason: string;
}
