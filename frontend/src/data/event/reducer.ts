import { createReducer } from 'core/redux';
import { ActionType } from 'data/actionTypes';
import { Visibility } from 'data/enum';

import { EventType } from './model';

interface EventInitialState {
  eventsList: EventType[];
  currentMode: Visibility;
  allEvents: EventType[];
}

const initialState: () => EventInitialState = () => ({
  eventsList: [],
  currentMode: Visibility.Pending,
  allEvents: []
});

export const eventReducer = createReducer(initialState, {
  [ActionType.EVENT_SETEVENTS]: 'eventsList',
  [ActionType.EVENT_SETCURRENTMODE]: 'currentMode',
  [ActionType.EVENT_GETALLASYNC]: 'allEvents'
});
