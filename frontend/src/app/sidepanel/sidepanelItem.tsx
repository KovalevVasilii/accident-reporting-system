import React from 'react';
import { Line } from 'shared/base/line';
import { Icon, ImportedIcon } from 'shared/base/icon';

import './sidepanelItem.scss';

export interface Props {
  title: string;
  icon: ImportedIcon;
  isActive?: boolean;
}

export const SidepanelItem: React.FC<Props> = ({ title, icon, isActive }) => {
  return (
    <Line className={`sidepanel-item ${isActive ? 'active' : ''}`} alignItems="center">
      <Icon name={icon}></Icon>
      <div className="title">{title}</div>
    </Line>
  );
};
