import React, { useState, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { Card } from 'shared/base/card';
import { Modal } from 'shared/base/modal';
import { Button } from 'shared/base/button';
import { Toggle } from 'app/toggle';
import { EventType } from 'data/event/model';
import { approveEventAsync } from 'data/event/action';
import { Status } from 'data/enum';

import './applicationsList.scss';

interface Props {
  events: EventType[];
}

export const ApplicationsList: React.FC<Props> = ({ events }) => {
  const [showModal, setShowModal] = useState<boolean>(false);

  const dispatch = useDispatch();
  const onApprove = useCallback((event: EventType) => {
    dispatch(approveEventAsync({
      id: event.id,
      status: Status.In_progress
    }));
  }, [dispatch]);

  const footer = (
    <>
      <Button onClick={() => setShowModal(false)} className="btn-outline-primary btn-sm">
        Отмена
      </Button>
      <Button onClick={() => setShowModal(false)} className="btn-primary btn-sm">
        Подтвердить
      </Button>
    </>
  );

  return (
    <div className="applicationsList">
      <Toggle />
      {events.map((x, i) => (
        <Card key={i} event={x}
          onEdit={() => { }}
          onDelete={() => { }}
          onApprove={() => onApprove(x)}></Card>
      ))}
      {showModal && (
        <Modal header="Подтверждение заявки" onCancel={() => setShowModal(false)} footer={footer}>
          <div className="text-modal">Вы уверены что хотите подтвердить данную заявку?</div>
        </Modal>
      )}
    </div>
  );
};