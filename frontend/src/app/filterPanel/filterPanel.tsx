import React, { useState, useEffect, useCallback } from 'react';
import { Line } from 'shared/base/line';
import { Block } from 'shared/base/block';
import { Checkbox } from 'shared/base/checkbox';
import { DateInputField } from 'shared/base/dateInputField';
import { SimpleSelectField } from 'shared/fields/SimpleSelectField';
import { type, status, priority } from 'app/translations';
import { EventType } from 'data/event/model';
import { Status, Priority, ServiceType } from 'data/enum';
import { useSelector } from 'react-redux';
import { StoreType } from 'core/store';

import './filterPanel.scss';

interface FilterType {
  electricity?: boolean;
  water?: boolean;
  gas?: boolean;
  heat?: boolean;
  date?: Date;
  status?: Status;
  priority?: Priority;
}

interface Props {
  onChange: (events: EventType[]) => void;
}

export const FilterPanel: React.FC<Props> = ({ onChange }) => {
  const events = useSelector((state: StoreType) => state.event.eventsList);
  const mode = useSelector((state: StoreType) => state.event.currentMode);
  const [filter, setFilter] = useState<FilterType>({});

  useEffect(() => {
    const prevState = [...events];
    const newState = prevState.filter(x => checkItem(x));
    onChange(newState);
  }, [events, filter, mode]);

  const checkItem = (v: EventType) => {
    return checkType(v.type) &&
      checkDate(new Date(v.created_at)) &&
      checkPriority(v.priority) &&
      checkStatus(v.status);
  };

  const checkType = useCallback((v: ServiceType) => {
    const { electricity, water, gas, heat } = filter;
    if (!electricity && !water && !gas && !heat) return true;

    return (electricity && v == ServiceType.Electricity) ||
      (water && v == ServiceType.Water) ||
      (gas && v == ServiceType.Gas) ||
      (heat && v == ServiceType.Heat)
      ? true : false;
  }, [filter]);

  const checkDate = useCallback((v: Date) => {
    const { date } = filter;
    if (!date) return true;
    return v.getTime() == date?.getTime();
  }, [filter]);

  const checkPriority = useCallback((v: Priority) => {
    const { priority } = filter;
    if (!priority) return true;
    return v == priority;
  }, [filter]);

  const checkStatus = useCallback((v: Status) => {
    const { status } = filter;
    if (!status) return true;
    return v == status;
  }, [filter]);

  return (
    <Line pr="4" mt="5" vertical className="filter-panel">
      <input type="text" className="form-control" placeholder="Поиск"></input>
      <Block className="title" mt="4">
        Фильтры
      </Block>
      <Checkbox
        className="title"
        text={type.get('electricity')}
        value={filter.electricity}
        onChange={(v: boolean) => setFilter({ ...filter, electricity: v })}></Checkbox>
      <Checkbox
        className="title"
        text={type.get('water')}
        value={filter.water}
        onChange={(v: boolean) => setFilter({ ...filter, water: v })}></Checkbox>
      <Checkbox
        className="title"
        text={type.get('gas')}
        value={filter.gas}
        onChange={(v: boolean) => setFilter({ ...filter, gas: v })}></Checkbox>
      <Checkbox
        className="title"
        text={type.get('heat')}
        value={filter.heat}
        onChange={(v: boolean) => setFilter({ ...filter, heat: v })}></Checkbox>
      <Block mt="4">
        <DateInputField value={new Date()} onChange={(v: Date) => setFilter({ ...filter, date: v })}></DateInputField>
      </Block>
      <Block className="title" mt="2">
        Статус
      </Block>
      <Block mt="2">
        <SimpleSelectField
          addEmptyOption
          getLabel={x => x}
          name="Status"
          options={status}
          value={filter.status}
          onChange={(v: Status) => setFilter({ ...filter, status: v })}></SimpleSelectField>
      </Block>
      <Block className="title">Уровень</Block>
      <Block mt="2">
        <SimpleSelectField
          addEmptyOption
          getLabel={x => x}
          name="Priority"
          options={priority}
          value={filter.priority}
          onChange={(v: Priority) => setFilter({ ...filter, priority: v })}></SimpleSelectField>
      </Block>
    </Line>
  );
};