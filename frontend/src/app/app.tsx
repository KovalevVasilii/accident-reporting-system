import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { StoreType } from 'core/store';
import { Sidepanel } from 'app/sidepanel/sidepanel';
import { Route, Switch } from 'react-router-dom';
import { Line } from 'shared/base/line';
import { Applications } from 'app/applications';
import { MapPage } from 'app/mapPage';
import { getEventsAsync, getAllAsync } from 'data/event/action';
import { EditPage } from 'app/editPage';

import './app.scss';

export const App: React.FC = () => {
  const dispatch = useDispatch();
  const mode = useSelector((state: StoreType) => state.event.currentMode);

  const getEvents = useCallback(() => {
    dispatch(getEventsAsync(mode));
    dispatch(getAllAsync());
  }, [dispatch, mode]);
  useEffect(() => getEvents(), [getEvents]);

  return (
    <Line className="app">
      <div className="sidepanel">
        <Sidepanel />
      </div>
      <div className="main-container">
        <Switch>
          <Route path="/applications" component={Applications} />
          <Route path="/edit/:id" render={props => <EditPage {...props}></EditPage>}></Route>
          <Route path="/map" component={MapPage} />
          <Route path="/" component={Applications} />
        </Switch>
      </div>
    </Line>
  );
};