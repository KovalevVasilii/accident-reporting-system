import React, { useState, useLayoutEffect } from 'react';
import { useSelector } from 'react-redux';
import { Line } from 'shared/base/line';
import { ApplicationsList } from 'app/applicationsList';
import { FilterPanel } from 'app/filterPanel/filterPanel';
import { StoreType } from 'core/store';
import { EventType } from 'data/event/model';

export const Applications: React.FC = () => {
  const initialEvents = useSelector((state: StoreType) => state.event.eventsList);
  const [filterEvents, setFilterEvents] = useState<EventType[]>([]);
  useLayoutEffect(() => setFilterEvents(initialEvents), [initialEvents]);

  return (
    <Line>
      <ApplicationsList events={filterEvents} />
      <FilterPanel onChange={(events: EventType[]) => setFilterEvents(events)} />
    </Line>
  );
};