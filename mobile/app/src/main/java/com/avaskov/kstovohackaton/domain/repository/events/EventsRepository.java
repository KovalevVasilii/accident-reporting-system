package com.avaskov.kstovohackaton.domain.repository.events;

import com.avaskov.kstovohackaton.domain.models.Event;
import com.avaskov.kstovohackaton.domain.models.SendingEvent;
import com.avaskov.kstovohackaton.domain.repository.RequestService;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;

import static com.avaskov.kstovohackaton.domain.StringsResources.BASE_URL;
import static com.avaskov.kstovohackaton.domain.StringsResources.EVENT_URL;
import static com.avaskov.kstovohackaton.domain.StringsResources.ID_PARAM;

public class EventsRepository {

    private RequestService requestService;

    public EventsRepository(RequestService requestService) {
        this.requestService = requestService;
    }

    public List<Event> getEventList() throws IOException, JSONException, ParseException {
        return requestService.getRequestForList(BASE_URL + EVENT_URL, Event.class);
    }

    public void saveEvent(SendingEvent sendingEvent) throws IOException {
        Gson gson = new Gson();
        requestService.postRequest(gson.toJson(sendingEvent, SendingEvent.class), BASE_URL + EVENT_URL);
    }

    public List<Event> getMyEvents(String userid) throws IOException, JSONException, ParseException {
        return requestService.getRequestForList(BASE_URL + EVENT_URL + ID_PARAM + userid, Event.class);
    }
}
