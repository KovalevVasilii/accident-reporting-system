package com.avaskov.kstovohackaton.domain.models;

public class SendingEvent {
    private String description;
    private int type;
    private double lon;
    private double lat;

    public SendingEvent(String description, int type, double lon, double lat) {
        this.description = description;
        this.type = type;
        this.lon = lon;
        this.lat = lat;
    }

    public String getDescription() {
        return description;
    }

    public int getType() {
        return type;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }
}
