package com.avaskov.kstovohackaton.domain.models;

import com.google.gson.annotations.SerializedName;

public class Event {

    public enum Priority {
        @SerializedName("very_low")
        VERYLOW,

        @SerializedName("low")
        LOW,

        @SerializedName("medium")
        MIDDLE,

        @SerializedName("high")
        HIGH,

        @SerializedName("critical")
        VERYHIGH
    }

    public enum Type {
        @SerializedName("gas")
        GAS,

        @SerializedName("water")
        WATER,

        @SerializedName("electricity")
        ELECTRICITY;
    }

    public enum Status {
        @SerializedName("pending")
        PENDING,

        @SerializedName("postponed")
        POSTPONED,

        @SerializedName("in_progress")
        INPROGRESS,

        @SerializedName("investigation")
        INVESTIGATION,

        @SerializedName("resolved")
        RESOLVED;
    }

    @SerializedName("id")
    private int id;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("type")
    private Type type;

    @SerializedName("status")
    private Status status;

    @SerializedName("start_time")
    private String startTime;

    @SerializedName("deadline")
    private String deadline;

    @SerializedName("priority")
    private Priority priority;

    @SerializedName("description")
    private String description;

    @SerializedName("lat")
    private double latitude;

    @SerializedName("lon")
    private double longitude;


    public Event(int id,
                 String createdAt,
                 Type type,
                 Status status,
                 String deadline,
                 Priority priority,
                 String description,
                 double latitude,
                 double longitude,
                 String startTime) {
        this.id = id;
        this.createdAt = createdAt;
        this.type = type;
        this.status = status;
        this.deadline = deadline;
        this.priority = priority;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.startTime = startTime;
    }

    public int getId() {
        return id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public Type getType() {
        return type;
    }

    public Status getStatus() {
        return status;
    }

    public String getDeadline() {
        return deadline;
    }

    public Priority getPriority() {
        return priority;
    }

    public String getDescription() {
        return description;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getStartTime() {
        return startTime;
    }
}
