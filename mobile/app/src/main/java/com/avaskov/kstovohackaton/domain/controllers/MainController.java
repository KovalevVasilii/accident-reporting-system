package com.avaskov.kstovohackaton.domain.controllers;

import android.util.Log;

import com.avaskov.kstovohackaton.domain.executor.Executor;
import com.avaskov.kstovohackaton.domain.executor.MainThread;
import com.avaskov.kstovohackaton.domain.executor.ThreadExecutor;
import com.avaskov.kstovohackaton.domain.models.Event;
import com.avaskov.kstovohackaton.domain.models.SendingEvent;
import com.avaskov.kstovohackaton.domain.repository.RequestService;
import com.avaskov.kstovohackaton.domain.repository.events.EventsRepository;
import com.avaskov.kstovohackaton.threading.MainThreadImpl;
import com.avaskov.kstovohackaton.ui.activities.MainView;

import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainController {
    private static final String TAG = "MAIN_CONTROLLER";

    private Executor executor;
    private MainThread mainThread;
    private MainView view;
    private EventsRepository eventsRepository;

    private List<Event> events;

    public MainController(MainView view) {
        this.view = view;

        eventsRepository = new EventsRepository(new RequestService());
        executor = ThreadExecutor.getInstance();
        mainThread = MainThreadImpl.getInstance();

        events = new ArrayList<>();
        Log.i(TAG, "created");
    }

    public void viewIsReady() {
        executor.execute(() -> {
            try {
                List<Event> eventList = eventsRepository.getEventList();

                events.clear();

                events.addAll(eventList);

                Log.i(TAG, "event list obtained");
                mainThread.post(() -> view.showEventList(eventList));
            } catch (IOException| JSONException| ParseException e) {
                Log.e(TAG, e.getMessage());
                mainThread.post(() -> view.showError());
            }
        });
    }

    public void eventPressed(double latitude, double longitude) {
        executor.execute(() -> {
            for (Event event : events) {
                if (event.getLatitude() == latitude && event.getLongitude() == longitude) {
                    Log.i(TAG, String.format("event pressed id: %d", event.getId()));
                    mainThread.post(() -> view.showAdditionalInfo(event));
                }
            }
        });
    }

    public void addButtonPressed(String description, int type, double latitude, double longitude) {
        executor.execute(() -> {
            if (!description.isEmpty()) {
                try {
                    eventsRepository.saveEvent(new SendingEvent(description, type, longitude, latitude));

                    Log.i(TAG, "event was saved");
                    mainThread.post(() -> view.eventWasSaved());
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                    mainThread.post(() -> view.showError());
                }
            } else {
                Log.i(TAG, "description is empty");
                mainThread.post(() -> view.descriptionIsEmpty());
            }
        });
    }

    public void myEventPressed() {
        executor.execute(() -> {
            try {
                List<Event> events = eventsRepository.getMyEvents("1");

                Log.i(TAG, "shown self events");
                mainThread.post(() -> view.showEventList(events));
            } catch (IOException| JSONException| ParseException e) {
                Log.e(TAG, e.getMessage());
                mainThread.post(() -> view.showError());
            }
        });
    }
}
