package com.avaskov.kstovohackaton.domain.repository;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.avaskov.kstovohackaton.domain.StringsResources.APPLICATION_JSON;
import static com.avaskov.kstovohackaton.domain.StringsResources.CONTENT_TYPE;
import static com.avaskov.kstovohackaton.domain.StringsResources.GET;
import static com.avaskov.kstovohackaton.domain.StringsResources.POST;
import static com.avaskov.kstovohackaton.domain.StringsResources.RESPONSE_DATA;
import static com.avaskov.kstovohackaton.domain.StringsResources.RESPONSE_RESULT;
import static com.avaskov.kstovohackaton.domain.StringsResources.RESPONSE_RESULT_TRUE;

public class RequestService {
    private static final String TAG = "REQUEST_SERVICE";

    private JSONParser parser;
    private Gson gson;

    public RequestService() {
        gson = new Gson();
        parser = new JSONParser();
    }

    public <T> List<T> getRequestForList(String request, Class<T> T) throws IOException, JSONException, ParseException {
        URL urlForGetRequest = new URL(request);
        String readLine;
        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
        connection.setRequestMethod(GET);
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            while ((readLine = in.readLine()) != null) {
                response.append(readLine);
            }
            in.close();

            Log.d(TAG, String.format("JSON String Result: %s", response.toString()));

            return generateResult(response.toString(), T);
        } else {
            Log.i(TAG, "get request not work");
            throw new IOException();
        }
    }

    public <T> T getRequestForOneEntity(String request, Class<T> T) throws IOException {
        URL urlForGetRequest = new URL(request);
        String readLine;
        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
        connection.setRequestMethod(GET);
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            while ((readLine = in.readLine()) != null) {
                response.append(readLine);
            }
            in.close();

            Log.d(TAG, String.format("JSON String Result: %s", response.toString()));

            Gson gson = new Gson();
            return gson.fromJson(response.toString(), T);
        } else {
            Log.i(TAG, "get request for one entity not work");
            throw new IOException();
        }
    }

    public void postRequest(String json, String request) throws IOException {
        URL obj = new URL(request);

        HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
        postConnection.setRequestMethod(POST);
        postConnection.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
        postConnection.setDoOutput(true);

        OutputStream os = postConnection.getOutputStream();
        os.write(json.getBytes());
        os.flush();
        os.close();

        int responseCode = postConnection.getResponseCode();

        Log.d(TAG, String.format("POST Response Code : %s", responseCode));
        Log.d(TAG, String.format("POST Response Message : %s", postConnection.getResponseMessage()));

        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    postConnection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.d(TAG, String.format("response is: %s", response.toString()));
        } else {
            Log.i(TAG, "post not worked");
            throw new IOException();
        }
    }

    private <T> List<T> generateResult(String jsonString, Class<T> T) throws ParseException, JSONException {
        List<T> result = new ArrayList<>();
        JSONObject json = (JSONObject) parser.parse(jsonString);

        String responseResult = Objects.requireNonNull(json.get(RESPONSE_RESULT)).toString();
        if (responseResult.equals(RESPONSE_RESULT_TRUE)) {
            String dataJsonString = Objects.requireNonNull(json.get(RESPONSE_DATA)).toString();
            JSONArray jsonArray = new JSONArray(dataJsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                result.add(gson.fromJson(jsonArray.getString(i), T));
            }
            return result;
        } else {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_EXCEPTION);
        }
    }
}
