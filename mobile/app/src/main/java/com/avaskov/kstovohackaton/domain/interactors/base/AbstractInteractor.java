package com.avaskov.kstovohackaton.domain.interactors.base;

import com.avaskov.kstovohackaton.domain.executor.Executor;
import com.avaskov.kstovohackaton.domain.executor.MainThread;

public class AbstractInteractor {
    protected Executor threadExecutor;
    protected MainThread mainThread;

    public AbstractInteractor(Executor executor, MainThread mainThread) {
        threadExecutor = executor;
        this.mainThread = mainThread;
    }
}
