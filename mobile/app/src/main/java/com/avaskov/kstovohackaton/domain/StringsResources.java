package com.avaskov.kstovohackaton.domain;

public class StringsResources {
    public static final String BASE_URL = "http://spacehub.su/api";
    public static final String EVENT_URL = "/event";
    public static final String ID_PARAM = "?id_=";
    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON = "application/json";
    public static final String RESPONSE_RESULT = "ok";
    public static final String RESPONSE_RESULT_TRUE = "true";
    public static final String RESPONSE_DATA = "data";
}
