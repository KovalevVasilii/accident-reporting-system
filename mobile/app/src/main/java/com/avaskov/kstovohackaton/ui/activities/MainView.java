package com.avaskov.kstovohackaton.ui.activities;

import com.avaskov.kstovohackaton.domain.models.Event;

import java.util.List;

public interface MainView {
    void eventWasSaved();
    void descriptionIsEmpty();
    void showEventList(List<Event> events);
    void showAdditionalInfo(Event event);
    void showError();
}
