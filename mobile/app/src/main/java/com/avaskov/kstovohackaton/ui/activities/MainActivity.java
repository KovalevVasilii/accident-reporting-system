package com.avaskov.kstovohackaton.ui.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.avaskov.kstovohackaton.R;
import com.avaskov.kstovohackaton.domain.controllers.MainController;
import com.avaskov.kstovohackaton.domain.models.Event;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends FragmentActivity implements MainView {

    private static final String TAG = "MAIN_ACTIVITY";

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @BindView(R.id.additional_info_event_rl)
    RelativeLayout additionalLayout;
    @BindView(R.id.additional_description_event_tv)
    TextView descriptionTextView;
    @BindView(R.id.additional_priority_event_iv)
    ImageView priorityImageView;
    @BindView(R.id.start_time_tv)
    TextView startTimeTextView;
    @BindView(R.id.additional_finish_time_tv)
    TextView finishTimeTextView;
    @BindView(R.id.additional_status_event)
    TextView statusTextView;
    @BindView(R.id.add_event_rl)
    RelativeLayout addEventLayout;
    @BindView(R.id.add_description_et)
    EditText addDescriptionEditText;
    @BindView(R.id.type_spinner)
    Spinner typeSpinner;

    private Map map = null;
    private AndroidXMapFragment mapFragment = null;
    private PositioningManager posManager;
    private GeoPosition centerPosition;
    private GeoCoordinate longPressedLastCoordinate;

    private MainController controller;
    private Unbinder unbinder;

    private List<MapObject> markers;

    private boolean isMyEventsNow;

    private PositioningManager.OnPositionChangedListener positionListener = new
            PositioningManager.OnPositionChangedListener() {

                public void onPositionUpdated(PositioningManager.LocationMethod method,
                                              GeoPosition position, boolean isMapMatched) {
                    centerPosition = position;
                }

                public void onPositionFixChanged(PositioningManager.LocationMethod method,
                                                 PositioningManager.LocationStatus status) {
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        checkPermissions();

        posManager = PositioningManager.getInstance();
        posManager.addListener(new WeakReference<>(positionListener));
        if (posManager != null) {
            posManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
        }

        controller = new MainController(this);

        markers = new ArrayList<>();

        additionalLayout.setVisibility(View.GONE);
        addEventLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();

        onResume();
    }

    public void onResume() {
        super.onResume();

        if (posManager != null) {
            posManager.start(
                    PositioningManager.LocationMethod.GPS_NETWORK);
        } else {
            posManager = PositioningManager.getInstance();
            posManager.addListener(new WeakReference<>(positionListener));
            if (posManager != null) {
                posManager.start(
                        PositioningManager.LocationMethod.GPS_NETWORK);
            }
        }
    }

    public void onPause() {
        if (posManager != null) {
            posManager.stop();
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        unbinder.unbind();
        onPause();
    }

    private void initialize() {
        mapFragment = (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);

        mapFragment.init(error -> {
            if (error == OnEngineInitListener.Error.NONE) {
                map = mapFragment.getMap();
                map.getPositionIndicator().setVisible(true);
                map.setCenter(new GeoCoordinate(56.138909, 44.341099, 0.0), //TODO: set center after first get position
                        Map.Animation.NONE);
                map.setZoomLevel(map.getMaxZoomLevel()); //TODO: set normal zoom

                setMapGestureListener();
                controller.viewIsReady();
            } else {
                Log.e(TAG,"Cannot initialize Map Fragment");
            }
        });
    }

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<>();

        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }

        if (!missingPermissions.isEmpty()) {
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, R.string.grand_permissions, Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                initialize();
                break;
        }
    }

    public void showLostConnection() {
        Toast.makeText(this, R.string.lost_connection, Toast.LENGTH_SHORT).show();
    }

    private void setMapGestureListener() {
        MapGesture.OnGestureListener listener =
                new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
                    @Override
                    public boolean onMapObjectsSelected(List<ViewObject> objects) {
                        for (ViewObject viewObj : objects) {
                            if (viewObj.getBaseType() == ViewObject.Type.USER_OBJECT) {
                                if (((MapObject) viewObj).getType() == MapObject.Type.MARKER) {
                                    MapMarker marker = (MapMarker) viewObj;
                                    controller.eventPressed(marker.getCoordinate().getLatitude(), marker.getCoordinate().getLongitude());
                                    return true;
                                }
                            }
                        }

                        return false;
                    }

                    @Override
                    public boolean onLongPressEvent(PointF pointF) {
                        longPressedLastCoordinate = map.pixelToGeo(pointF);

                        addEventLayout.setVisibility(View.VISIBLE);

                        return true;
                    }

                    @Override
                    public boolean onTapEvent(PointF pointF) {
                        addEventLayout.setVisibility(View.GONE);
                        additionalLayout.setVisibility(View.GONE);

                        return true;
                    }

                    @Override
                    public void onPanStart() {
                        super.onPanStart();
                        if (posManager == null) {
                            posManager = PositioningManager.getInstance();
                            posManager.addListener(new WeakReference<>(positionListener));
                            if (posManager != null) {
                                posManager.start(
                                        PositioningManager.LocationMethod.GPS_NETWORK);
                            }
                        }

                    }
                };

        mapFragment.getMapGesture().addOnGestureListener(listener);
    }

    public void showEventList(List<Event> eventList) {
        map.removeMapObjects(markers);
        markers.clear();
        for (Event event : eventList) {
            MapMarker mapMarker = new MapMarker(new GeoCoordinate(event.getLatitude(), event.getLongitude()));
            mapMarker.showInfoBubble();
            try {
                if (event.getType() == Event.Type.GAS) {
                    Image icon = new Image();
                    icon.setImageResource(R.drawable.gas);
                    mapMarker.setIcon(icon);
                } else if (event.getType() == Event.Type.WATER) {
                    Image icon = new Image();
                    icon.setImageResource(R.drawable.water);
                    mapMarker.setIcon(icon);
                } else if (event.getType() == Event.Type.ELECTRICITY) {
                    Image icon = new Image();
                    icon.setImageResource(R.drawable.lightning);
                    mapMarker.setIcon(icon);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            map.addMapObject(mapMarker);
            markers.add(mapMarker);
        }
    }

    public void showAdditionalInfo(Event event) {
        descriptionTextView.setText(event.getDescription());

        switch (event.getPriority()) {
            case VERYLOW:
                priorityImageView.setImageResource(R.drawable.ic_priority_1);
                break;
            case LOW:
                priorityImageView.setImageResource(R.drawable.ic_priority_2);
                break;
            case MIDDLE:
                priorityImageView.setImageResource(R.drawable.ic_priority_3);
                break;
            case HIGH:
                priorityImageView.setImageResource(R.drawable.ic_priority_4);
                break;
            case VERYHIGH:
                priorityImageView.setImageResource(R.drawable.ic_priority_5);
                break;
        }

        startTimeTextView.setText(event.getStartTime());
        finishTimeTextView.setText(event.getDeadline());

        switch (event.getStatus()) {
            case PENDING:
                statusTextView.setText(R.string.pending);
                break;
            case POSTPONED:
                statusTextView.setText(R.string.postponed);
                break;
            case INPROGRESS:
                statusTextView.setText(R.string.in_progress);
                break;
            case INVESTIGATION:
                statusTextView.setText(R.string.investigation);
                break;
            case RESOLVED:
                statusTextView.setText(R.string.resolved);
                break;
        }

        additionalLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show();
    }

    public void eventWasSaved() {
        addDescriptionEditText.setText(R.string.empty_text);
        addEventLayout.setVisibility(View.GONE);
        controller.viewIsReady();
    }

    public void descriptionIsEmpty() {
        Toast.makeText(this, R.string.fill_description, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.add_button)
    public void addEventButtonPressed() {
        controller.addButtonPressed(addDescriptionEditText.getText().toString(), typeSpinner.getSelectedItemPosition() + 1,
                longPressedLastCoordinate.getLatitude(), longPressedLastCoordinate.getLongitude());
    }

    @OnClick(R.id.near_me_button)
    public void nearMePressed() {
        if (centerPosition != null) {
            map.setCenter(centerPosition.getCoordinate(), Map.Animation.LINEAR);
        }
    }

    @OnClick(R.id.ok_button)
    public void okPressed() {
        additionalLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.my_events_button)
    public void myEventsPressed() {
        if (isMyEventsNow) {
            controller.viewIsReady();
        } else {
            controller.myEventPressed();
        }
        isMyEventsNow = !isMyEventsNow;
    }
}