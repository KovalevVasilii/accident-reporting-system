package com.avaskov.kstovohackaton.domain.interactors.base;

public interface Interactor {

    void execute();
}
