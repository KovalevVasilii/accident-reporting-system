package com.avaskov.kstovohackaton.domain.executor;

public interface Executor {

    void execute(Runnable interactor);
}