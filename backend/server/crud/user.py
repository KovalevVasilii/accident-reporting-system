from typing import List

from sqlalchemy.orm import Session

from database import User


def get_users(db: Session) -> List[User]:
    return db.query(User).all()


def get_user(db: Session, user_id: int) -> User:
    return db.query(User).filter(User.id == user_id).first()
