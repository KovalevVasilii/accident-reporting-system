import datetime
from enum import Enum
from typing import List, Optional, Union

from fastapi import File, UploadFile
from geoalchemy2 import Geometry
from geopy.geocoders import Nominatim
from sqlalchemy import cast, func, or_
from sqlalchemy.orm import Query, Session

from conf import service_settings
from conf.planning_config import config
from database import Event
from database.model_schemas import EventBase, EventPatch
from database.models import Priority, ServiceType, Status
from database.models.schemas import Visibility
from utils.excel_parser import ExcelParser
from tempfile import SpooledTemporaryFile
from io import BytesIO


def get_events(
    db: Session,
    visibility: Visibility = Visibility.ACTUAL,
) -> Query:
    if visibility == Visibility.ACTUAL:
        query = db.query(Event).filter(
            ~Event.status.in_([Status.Pending.value, Status.Resolved.value, Status.Rejected.value]),
            or_(
                Event.resolved_at > datetime.datetime.utcnow(),
                Event.resolved_at == None,  # pylint: disable=C0121
            ),
        )
    elif visibility == Visibility.PENDING:
        query = db.query(Event).filter(Event.status == Status.Pending.value)
    else:
        query = db.query(Event)
    return query


def get_events_by_user_id(
    db: Session,
    id: int,  # pylint: disable=W0622
    visibility: Visibility = Visibility.ACTUAL,
) -> Query:
    return (
        get_events(db, visibility)
        .filter(Event.user_id == id)
    )


def get_events_by_type(
    db: Session,
    type_: ServiceType,
    visibility: Visibility = Visibility.ACTUAL,
) -> Query:
    return (
        get_events(db, visibility)
        .filter(Event.type == type_.value)
    )


def get_event(db: Session, event_id: int) -> Event:
    return db.query(Event).filter(Event.id == event_id).first()


def update_event(db: Session, patch_event: EventPatch) -> Event:
    values_dict = patch_event.dict(skip_defaults=True)
    long = values_dict.pop('lon', None)
    lat = values_dict.pop('lat', None)

    status = values_dict.get('status', None)

    for key in values_dict:
        if isinstance(values_dict[key], Enum):
            values_dict[key] = values_dict[key].value

    event_query = db.query(Event).filter(
        Event.id == patch_event.id
    )  # FIXME: make it a single query
    event = event_query.first()

    if lat or long:
        event.set_lat_lon(lat, long)
        db.add(event)

    if status in [Status.Resolved, Status.Rejected]:
        event.resolved_at = datetime.datetime.utcnow()
        db.add(event)

    db.query(Event).filter(Event.id == event.id).update(values_dict)

    db.commit()
    db.refresh(event)

    return event


def create_event(db: Session, event: EventBase, user_id: Optional[int] = None) -> Event:
    db_event = Event(
        description=event.description,
        type=event.type.value,
        priority='',
        status='',
        position=f'POINT({event.lon} {event.lat})',
        user_id=user_id,
    )

    db_event.populate()
    if not event.priority:
        db_event.priority = Priority.VeryLow.value
    else:
        db_event.priority = event.priority.value

    if not event.status:
        db_event.status = Status.Pending.value
    else:
        db_event.status = event.status.value

    db.add(db_event)
    db.commit()
    db.refresh(db_event)

    return db_event


def event_already_exists(db: Session, event: EventBase) -> Optional[Event]:
    query = db.query(Event).filter(
        Event.type == event.type.value,
        Event.status != Status.Resolved.value,
        func.ST_DistanceSphere(
            cast(Event.position, Geometry), func.ST_MakePoint(event.lon, event.lat)
        )
        <= service_settings.EVENTS_PROXIMITY_THRESHOLD,
    )
    return query.first()


def increase_event_rating(db: Session, db_event: Event, increase: int = 1) -> Event:
    db_event.rating += increase
    db.add(db_event)
    db.commit()
    db.refresh(db_event)
    return db_event


def create_system_event(db: Session, event: dict) -> Event:
    db_event = Event(
        description=event['description'],
        type=event['type'],
        priority=Priority.VeryLow.value,
        status=Status.InProgress.value,
        position=event['position'],
        user_id=event['user_id'],
        start_time=event['start_time'],
        resolved_at=event['resolved_at'],
        is_system=True,
    )

    db_event.populate()

    db.add(db_event)
    db.commit()
    db.refresh(db_event)

    return db_event


def create_planned_events(db: Session, file: BytesIO, user_id: int, type: ServiceType):
    geolocator = Nominatim(user_agent='our_app')

    elements = ExcelParser.parse(file, True)
    events = []

    for element in elements:
        event = {}
        for key, aliases in config.items():
            for alias in aliases:
                v = element.pop(alias, None)
                if v is not None:
                    event[key] = v
        events.append(event)

    for event in events:
        event['type'] = type
        event['user_id'] = user_id
        address = event.pop('address', None)
        if address is not None:
            location = geolocator.geocode(address)
            event['position'] = f'POINT({location.longitude} {location.latitude})'

    return [create_system_event(db, event) for event in events]
