from typing import Dict, List, Union

from pydantic import BaseModel


class ErrorResponse(BaseModel):
    ok: bool = False
    error: Union[str, Dict, List] = 'Unknown error'
