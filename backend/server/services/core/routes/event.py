# pylint: disable=W0622

from typing import List, Union

from fastapi import Depends, File, Header, UploadFile
from geopy.exc import GeocoderServiceError
from sqlalchemy.orm import Session
from starlette.responses import Response

from conf import service_settings
from crud import create_event as create_event_
from crud import create_planned_events, event_already_exists
from crud import get_event as get_event_
from crud import get_events as get_events_
from crud import get_events_by_user_id, get_user, increase_event_rating
from crud import update_event as update_event_
from database import Event, EventBase, EventModel, EventPatch, ServiceType
from database.models import Status
from database.models.schemas import Visibility
from services.api import Error, responses
from services.dependencies import get_db
from io import BytesIO

from . import api


@api.get('/event', response_model=List[EventModel], responses=responses)
def events_list(
    id_: int = None,
    visibility: Visibility = None,
    db: Session = Depends(get_db),
    offset: int = 0,
    limit: int = service_settings.MAX_LIMIT,
) -> Union[Response, List[Event]]:
    if limit > service_settings.MAX_LIMIT:
        return Error(f'Maximum limit is {service_settings.MAX_LIMIT}!')
    if not visibility:
        visibility = Visibility.ACTUAL
    if id_ is not None:
        return get_events_by_user_id(db, id_, visibility).order_by(Event.id).offset(offset).limit(limit).all()
    return get_events_(db, visibility).order_by(Event.id).offset(offset).limit(limit).all()


@api.get('/event/{id}', response_model=EventModel)
def get_event(id: int, db: Session = Depends(get_db)) -> Event:
    return get_event_(db, id)


@api.patch('/event/{id}', response_model=EventModel, responses=responses)
def update_event(
    id: int, event_base: EventPatch, db: Session = Depends(get_db)
) -> Union[Response, Event]:
    if id is None:
        return Error('Param <id> is mandatory')
    event_base.id = id
    event = get_event_(db, id)
    if event is None:
        return Error(f'Event <{id}> does not exist')

    if (
        event_base.status
        and event_base.status == Status.Rejected
        and event_base.rejected_reason is None
    ):
        return Error('Field <rejected_reason> is mandatory for <Rejected> status')
    return update_event_(db, event_base)


@api.post('/event', response_model=EventModel, responses=responses)
def create_event(
    event: EventBase, db: Session = Depends(get_db), x_user_info: str = Header(None)
) -> Union[Response, Event]:
    similar_event = event_already_exists(db, event)
    if similar_event is not None:
        return increase_event_rating(db, similar_event)
    try:
        user_id = int(x_user_info)
        user = get_user(db, user_id)
        if not user:
            return Error(f'User <{user_id}> does not exist')
        return create_event_(db=db, event=event, user_id=int(x_user_info))
    except Exception as exc:  # pylint: disable=W0703
        return Error(str(exc.args))


@api.post('/event/plan', response_model=List[EventModel], responses=responses)
def events_planned_list(
    operator_id: int = None, file: UploadFile = File(...), db: Session = Depends(get_db)
):
    # TODO: filter by operator_id
    # TODO: get type from operator_id
    type = ServiceType.Gas.value

    try:
        return create_planned_events(db, BytesIO(file.file.read()), operator_id, type)
    except GeocoderServiceError:
        return Error('Something went wrong. Try again')
    except KeyError:
        return Error('Wrong input file format')
