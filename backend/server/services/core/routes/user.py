from typing import List

from fastapi import Depends
from sqlalchemy.orm import Session

from crud import get_users
from database import User, UserModel
from services.dependencies import get_db

from . import api


@api.get('/user', response_model=List[UserModel])
def users_list(db: Session = Depends(get_db)) -> List[User]:
    return get_users(db)
