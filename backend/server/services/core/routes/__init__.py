from services.api import Api

api: Api = Api()

from .event import create_event, update_event  # pylint: disable=C0413  # isort:skip
from .user import users_list  # pylint: disable=C0413  # isort:skip
