import datetime
from typing import Optional

from pydantic import BaseModel

from database.models.models import Priority, ServiceType, Status


class ORMModel(BaseModel):
    class Config:
        orm_mode = True


class UserBase(ORMModel):
    name: str


class UserModel(UserBase):
    id: int


class EventBase(ORMModel):
    type: ServiceType
    lon: float
    lat: float
    description: Optional[str]
    priority: Optional[Priority]
    status: Optional[Status]


class EventPatch(ORMModel):
    id: Optional[int]
    description: Optional[str]
    type: Optional[ServiceType]
    lon: Optional[float]
    lat: Optional[float]
    priority: Optional[Priority]
    status: Optional[Status]
    is_system: Optional[bool]
    rejected_reason: Optional[str]


class EventModel(EventBase):
    id: int
    created_at: datetime.datetime
    deadline: Optional[datetime.datetime]
    start_time: Optional[datetime.datetime]
    resolved_at: Optional[datetime.datetime]
    user: Optional[UserModel]
    rating: int
    is_system: bool
    rejected_reason: Optional[str]
