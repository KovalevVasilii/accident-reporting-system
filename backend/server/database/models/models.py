import datetime
import enum

from geoalchemy2 import Geography
from geoalchemy2.shape import to_shape
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, orm
from sqlalchemy.orm import relationship

from database import Base


class Status(enum.Enum):
    Pending = 'pending'
    Postponed = 'postponed'
    InProgress = 'in_progress'
    Investigation = 'investigation'
    Resolved = 'resolved'
    Rejected = 'rejected'


class ServiceType(enum.Enum):
    Gas = 'gas'
    Water = 'water'
    Electricity = 'electricity'


class Priority(enum.Enum):
    VeryLow = 'very_low'
    Low = 'low'
    Medium = 'medium'
    High = 'high'
    Critical = 'critical'


class User(Base):
    __tablename__ = 'users'
    # System fields
    id = Column('user_id', Integer, primary_key=True, index=True)
    # User customization
    name = Column(String(50))

    def __repr__(self) -> str:
        return f'<User {self.id} : {self.name}'


class Event(Base):
    __tablename__ = 'events'
    # System fields
    id = Column('event_id', Integer, primary_key=True, index=True)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    # Main data
    description = Column(String(250))
    type = Column(String(50))
    priority = Column(String(50))
    status = Column(String(50))
    # Times
    deadline = Column(DateTime, nullable=True, default=None)
    start_time = Column(DateTime, nullable=True, default=None)
    resolved_at = Column(DateTime, nullable=True, default=None)
    # User
    user_id = Column(Integer, ForeignKey(User.id), nullable=True)
    user = relationship(User, backref='events')
    # Geo data
    position = Column(Geography('POINT', srid=4326), nullable=False)
    lon = None
    lat = None
    # User rating
    rating = Column(Integer, nullable=False, default=1)
    is_system = Column(Boolean, nullable=False, default=False)

    rejected_reason = Column(String(250), nullable=True)

    def set_lat_lon(self, lat: float = None, lon: float = None) -> None:
        lat = lat or self.lat
        lon = lon or self.lon
        self.position = f'POINT({lon} {lat})'
        self.lat = lat
        self.lon = lon

    def populate(self) -> None:
        if not isinstance(self.position, str):
            point = to_shape(self.position)
            self.lon = point.x
            self.lat = point.y
        else:
            positions = self.position[6 : len(self.position) - 1].split()
            self.lon = float(positions[0])
            self.lat = float(positions[1])

    @orm.reconstructor
    def init(self) -> None:
        self.populate()

    def __repr__(self) -> str:
        return f'<Event {self.id} : {self.priority} : {self.type}'
