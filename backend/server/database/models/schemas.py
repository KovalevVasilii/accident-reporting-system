from __future__ import annotations

import enum


class Visibility(enum.Enum):
    ALL = 'all'
    ACTUAL = 'actual'
    PENDING = 'pending'
