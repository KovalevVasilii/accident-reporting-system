from functools import partial

from fastapi import FastAPI, File, UploadFile
from io import BytesIO
from tempfile import SpooledTemporaryFile

from crud import (
    create_event,
    create_planned_events,
    get_events,
    get_events_by_type,
    increase_event_rating,
    update_event,
)
from database import EventBase, EventPatch, Priority, ServiceType, Session, Status
from database.models.schemas import Visibility

from ..utils import Event, User, save_objects


def default_events():
    return [
        Event(status=status.value)
        for status in (Status.Pending, Status.InProgress, Status.Resolved, Status.Rejected)
    ]


def default_user():
    return User()


def create_events(session, visibility):
    pending, actual, resolved, rejected = default_events()

    save_objects(session, pending, actual, resolved, rejected)
    return get_events(session, visibility=visibility)


def test_get_events_actual(session):
    events = create_events(session, visibility=Visibility.ACTUAL)

    assert events.count() == 1
    assert events[0].status == Status.InProgress.value


def test_get_events_pending(session):
    events = create_events(session, visibility=Visibility.PENDING)

    assert events.count() == 1
    assert events[0].status == Status.Pending.value


def test_get_events_all(session):
    events = create_events(session, Visibility.ALL)

    assert events.count() == 4


def test_get_events_by_type(session):
    events = default_events()
    events.append(Event(type=ServiceType.Gas.value))

    save_objects(session, *events)

    electricity_events_all = get_events_by_type(
        session, ServiceType.Electricity, visibility=Visibility.ALL
    )
    electricity_events_actual = get_events_by_type(
        session, ServiceType.Electricity, visibility=Visibility.ACTUAL
    )
    gas_events_all = get_events_by_type(session, ServiceType.Gas, visibility=Visibility.ALL)

    assert electricity_events_all.count() == 4
    assert electricity_events_actual.count() == 1
    assert gas_events_all.count() == 1


def test_update_event(session):
    event = Event(status=Status.Pending.value)
    save_objects(session, event)

    event_patch = EventPatch(id=event.id, type=ServiceType.Gas, lat=10)

    event = update_event(session, event_patch)

    assert event.id == event_patch.id
    assert event.type == event_patch.type.value
    assert event.lat == event_patch.lat


def test_create_event(session):
    new_event = EventBase(type=ServiceType.Gas, lat=10, lon=5)

    event = create_event(session, new_event)

    assert event.type == new_event.type.value
    assert event.lon == new_event.lon
    assert event.lat == new_event.lat


def test_increase_rating(session):
    event, _, _, _ = default_events()
    save_objects(session, event)

    increase_event_rating(session, event)

    assert event.rating == 2


def test_update_event_resolved(session):
    event = Event(status=Status.Pending.value)
    save_objects(session, event)

    event_patch = EventPatch(
        id=event.id, type=ServiceType.Gas, lat=10, status=Status.Resolved.value
    )

    event = update_event(session, event_patch)

    assert event.id == event_patch.id
    assert event.type == event_patch.type.value
    assert event.lat == event_patch.lat
    assert event.resolved_at is not None


def test_update_event_rejected(session):
    event = Event(status=Status.Pending.value)
    save_objects(session, event)

    event_patch = EventPatch(
        id=event.id, type=ServiceType.Gas, lat=10, status=Status.Rejected.value
    )

    event = update_event(session, event_patch)

    assert event.id == event_patch.id
    assert event.type == event_patch.type.value
    assert event.lat == event_patch.lat
    assert event.resolved_at is not None


def test_uploading_planned_events(session):
    user = default_user()
    events = create_planned_events(session, 'tests/crud/test_data/test.xlsx', user.id, ServiceType.Gas.value)

    assert len(events) == 5
