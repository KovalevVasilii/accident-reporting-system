from typing import List

from database import Base
from database import Event as Event_
from database import ServiceType, Session
from database import User as User_


def Event(*args, **kwargs):
    if 'type' not in kwargs:
        kwargs['type'] = ServiceType.Electricity.value
    if 'lat' not in kwargs:
        kwargs['lat'] = 56.07
    if 'lon' not in kwargs:
        kwargs['lon'] = 44.1782
    lat, lon = kwargs['lat'], kwargs['lon']
    event = Event_(*args, **kwargs)
    event.set_lat_lon(lat=lat, lon=lon)
    return event


def User(*args, **kwargs):
    if 'name' not in kwargs:
        kwargs['name'] = 'Ivan'
    return User_(*args, **kwargs)


def save_objects(session: Session, *args: List[Base]) -> None:
    for object in args:
        session.add(object)
    session.commit()
