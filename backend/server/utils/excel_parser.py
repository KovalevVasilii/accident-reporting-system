from openpyxl import load_workbook


class ExcelParser:
    @staticmethod
    def drop_na(elements, headers):
        should_remove = []
        for element in elements:
            should_drop = True
            for h in headers:
                if element[h] is not None:
                    should_drop = False
                    break
            if should_drop:
                should_remove.append(element)
        for el in should_remove:
            elements.remove(el)

    @staticmethod
    def parse(filename, is_drop_na: bool = False):
        wb = load_workbook(filename=filename)
        active_sheet = wb.active
        max_row = active_sheet.max_row
        max_col = active_sheet.max_column

        elements= []

        # rows and columns starts with 1 line
        headers = [active_sheet.cell(row=1, column=c).value for c in range(1, max_col + 1)]

        # starts with 2, to skip the headers line
        for r in range(2, max_row + 1):
            element = {}
            for c in range(1, max_col + 1):
                element[headers[c - 1]] = active_sheet.cell(row=r, column=c).value
            elements.append(element)

        if is_drop_na:
            ExcelParser.drop_na(elements, headers)

        return elements


if __name__ == '__main__':
    elements = ExcelParser.parse('test.xlsx', True)

    for i in elements:
        print(i)
