from math import sqrt


def distance(first_x: float, first_y: float, second_x: float, second_y: float) -> float:
    x = abs(first_x - second_x)
    y = abs(first_y - second_y)
    return sqrt(x ** 2 + y ** 2)


METERS_IN_DEG = 111.139


def angles_to_meters(angles: float) -> float:
    return angles * METERS_IN_DEG


def metres_to_angles(meters: float) -> float:
    return meters / METERS_IN_DEG


def distance_angle_points(
    first_x: float, first_y: float, second_x: float, second_y: float
) -> float:
    return angles_to_meters(distance(first_x, first_y, second_x, second_y))
